import numpy as np
from scipy.stats import linregress
import copy

filename = 'mote2_band.dat'
nbnd = 18

f = open(filename,'r')
w = open('sorted'+filename,'w')
commentline = f.readline()
w.write(commentline)
commentline = f.readline()
w.write(commentline)
commentline = f.readline()
w.write(commentline)
endpoints = commentline.split()

nseg = len(endpoints) - 2

segment = []
band = []
full_dispersion = []
prevdata = 'NOT EMPTY'

for line in f:
    data = line.strip()
    if data == '' and prevdata == '':
        full_dispersion.append(band)
        band = []
    elif data == '':
        band.append(segment)
        segment = []
    else:
        segment.append(map(float,data.split()))
    prevdata = data
f.close()

srtd = copy.deepcopy(full_dispersion)



def raw_sort_left(segment):
    tempomega = []
    tempslope = []
    for i in np.arange(0,nbnd):
        q = np.array(full_dispersion[i][segment][:]).T[0]
        omega = np.array(full_dispersion[i][segment][:]).T[1]
        fit = linregress(q[0:5],omega[0:5])
        tempomega.append(omega[0])
        tempslope.append(fit[0])

    #index = np.argsort(tempomega)
    index = np.lexsort((tempslope,tempomega))

    for i in range(0,nbnd):
        srtd[i][segment] = [x for x in full_dispersion[index[i]][segment]]



def raw_sort_right(segment):
    tempomega = []
    tempslope = []
    for i in np.arange(0,nbnd):
        q = np.array(full_dispersion[i][segment][:]).T[0]
        omega = np.array(full_dispersion[i][segment][:]).T[1]
        fit = linregress(q[-5:],omega[-5:])
        tempomega.append(omega[-1])
        tempslope.append(fit[0])

    index = np.argsort(tempomega)

    for i in range(0,nbnd):
        srtd[i][segment] = [x for x in full_dispersion[index[i]][segment]]


def rel_sort_left(segment):
    index = []
    remaining_bands = range(0,nbnd)
    for i in np.arange(0,nbnd):
        q = np.array(srtd[i][segment-1][:]).T[0]
        omega = np.array(srtd[i][segment-1][:]).T[1]
        fit = linregress(q[-5:],omega[-5:])
        targetomega = omega[-1]
        targetslope = fit[0]

        minerror = 100000000.0

        for myband in remaining_bands:
            q = np.array(full_dispersion[myband][segment][:]).T[0]
            omega = np.array(full_dispersion[myband][segment][:]).T[1]
            fit = linregress(q[0:5],omega[0:5])
            myomega,myslope = omega[0],fit[0]
            myerror = (1000.0*(myomega - targetomega)**2) + (myslope-targetslope)**2
            if myerror < minerror:
                chosenband = myband
                minerror = myerror

        index.append(chosenband)
        remaining_bands.pop(remaining_bands.index(chosenband))

    for i in np.arange(0,nbnd):
        srtd[i][segment] = [x for x in full_dispersion[index[i]][segment]]

raw_sort_right(0)
raw_sort_left(1)
rel_sort_left(2)
raw_sort_right(3)

for i in range(0,nbnd):
    for segment in range(0,nseg):
        for element in srtd[i][segment]:
            w.write('%f  %f \n'%(element[0], element[1]))
        w.write('\n')
    w.write('\n')
w.close()
